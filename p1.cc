#include <fstream>
#include <string>
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/internet-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/netanim-module.h"


using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("p1");


int main (int argc, char *argv[])
{
  std::string lat = "2ms";
  std::string rate = "500kb/s"; 
  bool enableFlowMonitor = false;


  CommandLine cmd;
  cmd.AddValue ("latency", "P2P link Latency in miliseconds", lat);
  cmd.AddValue ("rate", "P2P data rate in bps", rate);
  cmd.AddValue ("EnableMonitor", "Enable Flow Monitor", enableFlowMonitor);

  cmd.Parse (argc, argv);


  // Creamos los nodos solicitados
  NS_LOG_INFO ("Create nodes.");
  NodeContainer nodes; 
  nodes.Create(8);
  
  // n0 -----+       +----- n5
  //         |       |
  // n1 -----n3 --- n4----- n6
  //         |       |
  // n2 -----+       +----- n7
  
  NodeContainer connectionN0toN3 = NodeContainer (nodes.Get (0), nodes.Get (3)); //n0 <--> n3
  NodeContainer connectionN1toN3 = NodeContainer (nodes.Get (1), nodes.Get (3)); //n1 <--> n3
  NodeContainer connectionN2toN3 = NodeContainer (nodes.Get (2), nodes.Get (3)); //n2 <--> n3
  NodeContainer connectionN3toN4 = NodeContainer (nodes.Get (3), nodes.Get (4)); //n3 <--> n4
  NodeContainer connectionN5toN4 = NodeContainer (nodes.Get (5), nodes.Get (4)); //n5 <--> n4
  NodeContainer connectionN6toN4 = NodeContainer (nodes.Get (6), nodes.Get (4)); //n6 <--> n4
  NodeContainer connectionN7toN4 = NodeContainer (nodes.Get (7), nodes.Get (4)); //n7 <--> n4

  // Instalamos Internet Stack
  InternetStackHelper internet;
  internet.Install (nodes);

  // Creamos channels
  NS_LOG_INFO ("Create channels.");
  PointToPointHelper p2p;
  p2p.SetDeviceAttribute ("DataRate", StringValue (rate));
  p2p.SetChannelAttribute ("Delay", StringValue (lat));
  NetDeviceContainer d0d3 = p2p.Install (connectionN0toN3);
  NetDeviceContainer d1d3 = p2p.Install (connectionN1toN3);
  NetDeviceContainer d2d3 = p2p.Install (connectionN2toN3);
  NetDeviceContainer d3d4 = p2p.Install (connectionN3toN4);
  NetDeviceContainer d5d4 = p2p.Install (connectionN5toN4);
  NetDeviceContainer d6d4 = p2p.Install (connectionN6toN4);
  NetDeviceContainer d7d4 = p2p.Install (connectionN7toN4);

  // Aagregamos direcciones IP 
  NS_LOG_INFO ("Assign IP Addresses.");
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaceN0toN3 = ipv4.Assign (d0d3);

  ipv4.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaceN1toN3 = ipv4.Assign (d1d3);

  ipv4.SetBase ("10.1.3.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaceN2toN3 = ipv4.Assign (d2d3);

  ipv4.SetBase ("10.1.4.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaceN3toN4 = ipv4.Assign (d3d4);

  ipv4.SetBase ("10.1.5.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaceN5toN4 = ipv4.Assign (d5d4);
  
  ipv4.SetBase ("10.1.6.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaceN6toN4 = ipv4.Assign (d6d4);
  
  ipv4.SetBase ("10.1.7.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaceN7toN4 = ipv4.Assign (d7d4);
  

  NS_LOG_INFO ("Enable static global routing.");

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();


  // Crear un socket TCP en el nodo 0,2,3,4,5 y 7
  Ptr<Socket> socket0 = Socket::CreateSocket (nodes.Get (0), TypeId::LookupByName ("ns3::TcpSocketFactory"));
  Ptr<Socket> socket2 = Socket::CreateSocket (nodes.Get (2), TypeId::LookupByName ("ns3::TcpSocketFactory"));
  Ptr<Socket> socket3 = Socket::CreateSocket (nodes.Get (3), TypeId::LookupByName ("ns3::TcpSocketFactory"));
  Ptr<Socket> socket4 = Socket::CreateSocket (nodes.Get (4), TypeId::LookupByName ("ns3::TcpSocketFactory"));
  Ptr<Socket> socket5 = Socket::CreateSocket (nodes.Get (5), TypeId::LookupByName ("ns3::TcpSocketFactory"));
  Ptr<Socket> socket7 = Socket::CreateSocket (nodes.Get (7), TypeId::LookupByName ("ns3::TcpSocketFactory"));

  // Crear un socket UDP en el nodo 1 y 6
  Ptr<Socket> socket1 = Socket::CreateSocket (nodes.Get (1), TypeId::LookupByName ("ns3::UdpSocketFactory"));
  Ptr<Socket> socket6 = Socket::CreateSocket (nodes.Get (6), TypeId::LookupByName ("ns3::UdpSocketFactory"));


  NS_LOG_INFO ("Create Applications.");


  // Define las configuraciones comunes
  uint32_t port = 8080;
  uint32_t packetSize = 1000;
  DataRate dataRate("1Mbps");

  // Crear emisores
  OnOffHelper onoffHelperTcp("ns3::TcpSocketFactory", InetSocketAddress(interfaceN5toN4.GetAddress(0), port));
  onoffHelperTcp.SetAttribute("DataRate", DataRateValue(dataRate));
  onoffHelperTcp.SetAttribute("PacketSize", UintegerValue(packetSize));

  OnOffHelper onoffHelperTcp2("ns3::TcpSocketFactory", InetSocketAddress(interfaceN7toN4.GetAddress(0), port));
  onoffHelperTcp2.SetAttribute("DataRate", DataRateValue(dataRate));
  onoffHelperTcp2.SetAttribute("PacketSize", UintegerValue(packetSize));

  OnOffHelper onoffHelperUdp("ns3::UdpSocketFactory", InetSocketAddress(interfaceN6toN4.GetAddress(0), port));
  onoffHelperUdp.SetAttribute("DataRate", DataRateValue(dataRate));
  onoffHelperUdp.SetAttribute("PacketSize", UintegerValue(packetSize));

  // Instalar aplicaciones de emisor en los nodos correspondientes
  ApplicationContainer app1 = onoffHelperTcp.Install(nodes.Get(0));
  ApplicationContainer app2 = onoffHelperTcp2.Install(nodes.Get(2));
  ApplicationContainer app3 = onoffHelperUdp.Install(nodes.Get(1));

  // Definir los tiempos de inicio y fin de las aplicaciones
  double startTime = 1.0;
  double stopTime = 100.0;
  app1.Start(Seconds(startTime));
  app1.Stop(Seconds(stopTime));
  app2.Start(Seconds(startTime));
  app2.Stop(Seconds(stopTime));
  app3.Start(Seconds(startTime));
  app3.Stop(Seconds(stopTime));

  // Crear receptores
  PacketSinkHelper packetSinkHelperTcp("ns3::TcpSocketFactory", InetSocketAddress(Ipv4Address::GetAny(), port));
  PacketSinkHelper packetSinkHelperTcp2("ns3::TcpSocketFactory", InetSocketAddress(Ipv4Address::GetAny(), port));
  PacketSinkHelper packetSinkHelperUdp("ns3::UdpSocketFactory", InetSocketAddress(interfaceN6toN4.GetAddress(0), port));

  // Instalar aplicaciones de receptor en los nodos correspondientes
  ApplicationContainer sinkApps1 = packetSinkHelperTcp.Install(nodes.Get(4));
  ApplicationContainer sinkApps2 = packetSinkHelperTcp2.Install(nodes.Get(6));
  ApplicationContainer sinkApps3 = packetSinkHelperUdp.Install(nodes.Get(5));

  // Definir los tiempos de inicio y fin de las aplicaciones
  sinkApps1.Start(Seconds(0.0));
  sinkApps1.Stop(Seconds(stopTime));
  sinkApps2.Start(Seconds(0.0));
  sinkApps2.Stop(Seconds(stopTime));
  sinkApps3.Start(Seconds(0.0));
  sinkApps3.Stop(Seconds(stopTime));

  // Animaciones para NetAdmin
  AnimationInterface anim ("animation.xml");
  anim.SetConstantPosition(nodes.Get(0), 20, 10);
  anim.SetConstantPosition(nodes.Get(1), 20, 25);
  anim.SetConstantPosition(nodes.Get(2), 20, 40);
  anim.SetConstantPosition(nodes.Get(3), 40, 25);
  anim.SetConstantPosition(nodes.Get(4), 50, 25);
  anim.SetConstantPosition(nodes.Get(5), 70, 10);
  anim.SetConstantPosition(nodes.Get(6), 70, 25);
  anim.SetConstantPosition(nodes.Get(7), 70, 40);
  anim.EnablePacketMetadata(true);


  // Flow Monitor
  Ptr<FlowMonitor> flowmon;
  if (enableFlowMonitor)
    {
      FlowMonitorHelper flowmonHelper;
      flowmon = flowmonHelper.InstallAll ();
    }


  // Now, do the actual simulation.
  NS_LOG_INFO ("Run Simulation.");
  Simulator::Stop (Seconds(100.0));
  Simulator::Run ();
  if (enableFlowMonitor)
    {
	  flowmon->CheckForLostPackets ();
	  flowmon->SerializeToXmlFile("lab-2.flowmon", true, true);
    }
  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");
}
