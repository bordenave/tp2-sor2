#include <fstream>
#include <string>
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/internet-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/point-to-point-layout-module.h"
#include "ns3/netanim-module.h"


using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("p1.1");


int main (int argc, char *argv[])
{
  std::string lat = "2ms";
  std::string rate = "500kb/s"; 
  bool enableFlowMonitor = false;
  uint32_t    nLeftLeaf = 3;
  uint32_t    nRightLeaf = 3;
  uint32_t    nLeaf = 0; // If non-zero, number of both left and right
  std::string animFile = "dumbbell-animation.xml" ;  // Name of file for animation output
   
  CommandLine cmd;
  cmd.AddValue ("nLeftLeaf", "Number of left side leaf nodes", nLeftLeaf);
  cmd.AddValue ("nRightLeaf","Number of right side leaf nodes", nRightLeaf);
  cmd.AddValue ("nLeaf",     "Number of left and right side leaf nodes", nLeaf);
  cmd.AddValue ("animFile",  "File Name for Animation Output", animFile);


  
  // n2 -----+       +----- n5
  //         |       |
  // n3 -----n0 --- n1----- n6
  //         |       |
  // n4 -----+       +----- n7
  
  // Create the point-to-point link helpers
  PointToPointHelper pointToPointRouter;
  pointToPointRouter.SetDeviceAttribute  ("DataRate", StringValue ("10Mbps"));
  pointToPointRouter.SetChannelAttribute ("Delay", StringValue ("1ms"));
  PointToPointHelper pointToPointLeaf;
  pointToPointLeaf.SetDeviceAttribute    ("DataRate", StringValue ("10Mbps"));
  pointToPointLeaf.SetChannelAttribute   ("Delay", StringValue ("1ms"));

  PointToPointDumbbellHelper d (nLeftLeaf, pointToPointLeaf,
                                nRightLeaf, pointToPointLeaf,
                                pointToPointRouter);

  // Install Stack
  InternetStackHelper stack;
  d.InstallStack (stack);



  // Agregamos direcciones IP 
  d.AssignIpv4Addresses (Ipv4AddressHelper ("10.1.1.0", "255.255.255.0"),
                         Ipv4AddressHelper ("10.2.1.0", "255.255.255.0"),
                         Ipv4AddressHelper ("10.3.1.0", "255.255.255.0"));
  

  NS_LOG_INFO ("Create Applications.");

  // Install on/off app on all right side nodes
  OnOffHelper clientHelperUdp ("ns3::UdpSocketFactory", Address ());
  clientHelperUdp.SetAttribute ("OnTime", StringValue ("ns3::UniformRandomVariable"));
  clientHelperUdp.SetAttribute ("OffTime", StringValue ("ns3::UniformRandomVariable"));
  
  OnOffHelper clientHelperTcp ("ns3::TcpSocketFactory", Address ());
  clientHelperTcp .SetAttribute ("OnTime", StringValue ("ns3::UniformRandomVariable"));
  clientHelperTcp .SetAttribute ("OffTime", StringValue ("ns3::UniformRandomVariable"));
  
  ApplicationContainer clientApps;
  
  for (uint32_t i = 0; i < 3; ++i)
  {
  	if(i == 1){
  	         AddressValue remoteAddressUdp (InetSocketAddress (d.GetRightIpv4Address (i), 1000));
                 clientHelperUdp.SetAttribute ("Remote", remoteAddressUdp);
                 clientApps.Add (clientHelperUdp.Install (d.GetLeft (i)));
  	}else{
		 AddressValue remoteAddressTcp (InetSocketAddress (d.GetRightIpv4Address (i), 1000));
		 clientHelperTcp.SetAttribute ("Remote", remoteAddressTcp);
		 clientApps.Add (clientHelperTcp.Install (d.GetLeft (i)));
         }
  }
   
  clientApps.Start (Seconds (0.0));
  clientApps.Stop (Seconds (10.0));


  // Animaciones para NetAdmin
  d.BoundingBox (1, 1, 100, 100);
  AnimationInterface anim (animFile);
  

  NS_LOG_INFO ("Enable static global routing.");
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  
  // Flow Monitor
  Ptr<FlowMonitor> flowmon;
  if (enableFlowMonitor)
    {
      FlowMonitorHelper flowmonHelper;
      flowmon = flowmonHelper.InstallAll ();
  }


  // Now, do the actual simulation.
  NS_LOG_INFO ("Run Simulation.");
  Simulator::Stop (Seconds(100.0));
  Simulator::Run ();
  if (enableFlowMonitor)
    {
	  flowmon->CheckForLostPackets ();
	  flowmon->SerializeToXmlFile("lab-2.flowmon", true, true);
    }
  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");
}
